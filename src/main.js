
import React from 'react';
import ReactDOM from 'react-dom';
import ReactIntl from 'react-intl';

var IntlMixin = ReactIntl.IntlMixin;
var FormattedNumber = ReactIntl.FormattedNumber;

var App = React.createClass({
    mixins: [IntlMixin],
    render: function() {
        <div id="whatever">
            <span><FormattedNumber value={30.999999} format="USD" /></span>
        </div>
    }
});

var intlData = {
    "locales": "en-US",
    "formats": {
        "number": {
            "USD": {
                "style": "currency",
                "currency": "USD",
                "minimumFractionDigits": 2,
                "maximumFractionDigits": 2
            },
        }
    }
};

ReactDOM.render(
    <App {...intlData} />,
    document.getElementById('intl_test')
);