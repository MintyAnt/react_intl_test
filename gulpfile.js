var gulp = require('gulp');
var babelify = require('babelify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');

gulp.task('default', ['bundle']);

gulp.task('bundle', function() {
    return browserify({
            entries: './src/main.js',
            debug: true
        })
        .transform(babelify)
        .bundle()
        .on('error', function (err) { console.log('Error : ' + err.message); })
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('public'))
});

gulp.task('serve', function() {
    browserSync.init({
        port: 3000,
        server: {
            baseDir: './public/'
        }
    });
});