# Usage

1. Ensure nodejs, npm, and gulp are installed globally
2. npm install
3. gulp
4. Open public/index.html
5. Observe console

# Source

- Code is located in src/main